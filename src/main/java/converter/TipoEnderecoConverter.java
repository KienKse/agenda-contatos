package converter;

import javax.persistence.AttributeConverter;

import enumeracao.TipoEnderecoEnum;

public class TipoEnderecoConverter implements AttributeConverter<TipoEnderecoEnum, String> {
    @Override
    public String convertToDatabaseColumn(TipoEnderecoEnum attribute) {
        return attribute != null ? attribute.getSigla() : null;
    }

    @Override
    public TipoEnderecoEnum convertToEntityAttribute(String dbData) {
        return dbData  != null ? TipoEnderecoEnum.valorOfSigla(dbData ) : null;
    }
}
