package dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;

import domain.Endereco;
import enumeracao.EnumTool;

public class EnderecoDAO {

	/** Criação do dao - Para Teste de Criação atráves de um dao */

	private static EnderecoDAO instance;
	protected EntityManager entityManager;

	public static EnderecoDAO getInstance() {
		if (instance == null) {
			instance = new EnderecoDAO();
		}

		return instance;
	}

	private EnderecoDAO() {
		entityManager = getEntityManager();
	}

	private EntityManager getEntityManager() {
		EntityManagerFactory factory = Persistence
				.createEntityManagerFactory(EnumTool.UNIDADE_PERSISTENCIA_ATUAL.getTexto());
		if (entityManager == null) {
			entityManager = factory.createEntityManager();
		}

		return entityManager;
	}

	@SuppressWarnings("unchecked")
	public List<Endereco> findAll() {
		return entityManager.createQuery("FROM " + Endereco.class.getName()).getResultList();
	}

	public void persist(Endereco endereco) {
		try {
			entityManager.persist(endereco);
		} catch (Exception ex) {
			ex.printStackTrace();
			entityManager.getTransaction().rollback();
		}
	}

	public void persistOnMerge(Endereco endereco) {
		try {
			if (endereco != null) {
				merge(endereco);
			} else {
				persist(endereco);
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			entityManager.getTransaction().rollback();
		}
	}

	public void merge(Endereco endereco) {
		try {
			entityManager.merge(endereco);
		} catch (Exception ex) {
			ex.printStackTrace();
			entityManager.getTransaction().rollback();
		}
	}

	public void remove(Endereco endereco) {
		try {
//            Endereco enderecoRemoved = entityManager.find(Endereco.class, endereco.getId());
			entityManager.remove(endereco);
		} catch (Exception ex) {
			ex.printStackTrace();
			entityManager.getTransaction().rollback();
		}
	}

	public void removeById(final Long id) {
		try {
			Endereco endereco = getById(id);
			if (endereco != null) {
				remove(endereco);
			} else {
				System.out.println(EnumTool.NAO_FORAM_ENCONTRADOS_REGISTROS.getTexto());
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	public Endereco getById(Long id) {
		return entityManager.find(Endereco.class, id);
	}

	public List<Endereco> getByBairro(String bairro) {
		TypedQuery<Endereco> query = entityManager.createQuery("select e from Endereco e where e.bairro = :bairro",
				Endereco.class);
		query.setParameter("bairro", bairro);
		List<Endereco> enderecos = query.getResultList();
		System.out.println("Endereco do bairro " + bairro + ":");
		for (Endereco endereco : enderecos) {
			System.out.println(endereco.toString());
		}
		return enderecos;
	}

	/** Dúvida Logradouro o que seria considerado como logradouro */
	public List<Endereco> getAllByLogradouro(String logradouro) {
		TypedQuery<Endereco> query = entityManager
				.createQuery("select e from Endereco e where e.logradouro = :logradouro", Endereco.class);
		query.setParameter("logradouro", logradouro);
		List<Endereco> enderecos = query.getResultList();
		System.out.println("Endereco do logradouro " + logradouro + ":");

		for (Endereco endereco : enderecos) {
			System.out.println(endereco.toString());
		}
		return enderecos;
	}

	/** Dúvida Logradouro o que seria considerado como logradouro */
	public Endereco getByLogradouro(String logradouro) {
		TypedQuery<Endereco> query = entityManager
				.createQuery("select e from Endereco e where e.logradouro = :logradouro", Endereco.class);
		query.setParameter("logradouro", logradouro);
		Endereco endereco = query.getSingleResult();
		System.out.println("Endereco do logradouro " + logradouro + ": " + endereco.toString());
		return endereco;
	}

}
