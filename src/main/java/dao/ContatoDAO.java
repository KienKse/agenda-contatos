package dao;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;

import domain.Contato;
import domain.Endereco;
import enumeracao.EnumTool;

public class ContatoDAO {

	/** Criação do dao - Para Teste de Criação atráves de um dao */

	private static ContatoDAO instance;
	protected EntityManager entityManager;

	public static ContatoDAO getInstance() {
		if (instance == null) {
			instance = new ContatoDAO();
		}

		return instance;
	}

	public ContatoDAO() {
		entityManager = getEntityManager();
	}

	private EntityManager getEntityManager() {
		EntityManagerFactory factory = Persistence
				.createEntityManagerFactory(EnumTool.UNIDADE_PERSISTENCIA_ATUAL.getTexto());
		if (entityManager == null) {
			entityManager = factory.createEntityManager();
		}

		return entityManager;
	}

	@SuppressWarnings("unchecked")
	public List<Contato> findAll() {
		return entityManager.createQuery("FROM " + Contato.class.getName()).getResultList();
	}

	public void persist(Contato contato) {
		try {
			entityManager.persist(contato);
		} catch (Exception ex) {
			ex.printStackTrace();
			entityManager.getTransaction().rollback();
		}
	}

	public void persistOnMerge(Contato contato) {
		try {
			if (contato.getId() != null) {
				merge(contato);
			} else {
				persist(contato);
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			entityManager.getTransaction().rollback();
		}
	}

	public void merge(Contato contato) {
		try {
			entityManager.getTransaction().begin();
			entityManager.merge(contato);
			entityManager.getTransaction().commit();
		} catch (Exception ex) {
			ex.printStackTrace();
			entityManager.getTransaction().rollback();
		}
	}

	public void remove(Contato contato) {
		try {
			entityManager.getTransaction().begin();
			entityManager.remove(contato);
			entityManager.getTransaction().commit();
		} catch (Exception ex) {
			ex.printStackTrace();
			entityManager.getTransaction().rollback();
		}
	}

	public void removeById(final Long id) {
		try {
			Contato contato = getById(id);
			if (contato != null) {
				remove(contato);
			} else {
				System.out.println(EnumTool.NAO_FORAM_ENCONTRADOS_REGISTROS.getTexto());
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	public void removeByNome(final String nome) {
		try {
			Contato contato = getByNome(nome);
			if (contato != null) {
				remove(contato);
			} else {
//                System.out.println(EnumTool.NAO_FORAM_ENCONTRADOS_REGISTROS.getTexto());
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	/** Vários nomes em iguais em uma agenda */
	public List<Contato> getAllByNome(String nome) {
		TypedQuery<Contato> query = entityManager.createQuery("select e from Contato e where e.nome = :nome",
				Contato.class);
		query.setParameter("nome", nome);
		List<Contato> contatos = query.getResultList();
		System.out.println(EnumTool.CONTATO.getTexto() + " do nome " + nome + ":");
		for (Contato contato : contatos) {
			System.out.println(contato.toString());
		}
		return contatos;
	}

	public Contato getById(Long id) {
		Contato contato = entityManager.find(Contato.class, id);
		return contato != null ? contato : null;
	}

	public Contato getByIdAula(Long id) {
		TypedQuery<Contato> query = entityManager.createQuery("select c from Contato c where c.id = :id",
				Contato.class);
		query.setParameter("id", id);
		Contato contato = query.getSingleResult();
		System.out.println(EnumTool.CONTATO.getTexto() + ": " + contato.toString());
		return contato;
	}

	public Contato getByNome(String nome) {
		try {
			TypedQuery<Contato> query = entityManager.createQuery("select c from Contato c where c.nome = :nome",
					Contato.class);
			query.setParameter("nome", nome);
			Contato contato = query.getSingleResult();
			return contato;
		} catch (Exception e) {
			System.out.println(EnumTool.NAO_FORAM_ENCONTRADOS_REGISTROS.getTexto());
		}
		return null;
	}

	public boolean verificarEnderecoExistente(Contato contato) {
		try {
			TypedQuery<Endereco> query = entityManager.createQuery("select c.enderecos from Contato c where c.id = :id",
					Endereco.class);
			query.setParameter("id", contato.getId());
			List<Endereco> enderecos = query.getResultList();

			if(enderecos != null && !enderecos.isEmpty()) {
				listarEnderecos(enderecos);
				return true;
			}

		} catch (Exception e) {
			System.out.println(EnumTool.NAO_FORAM_ENCONTRADOS_REGISTROS.getTexto());
		}
		return false;
	}

	public void listarEnderecos(List<Endereco> lista) {
		for (Endereco endereco: lista) {
			System.out.println(endereco.toString());
		}
	}


}
