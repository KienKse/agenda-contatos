package dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;

import domain.Estado;
import enumeracao.EnumTool;

public class EstadoDAO {

	/** Criação do dao - Para Teste de Criação atráves de um dao */

	private static EstadoDAO instance;
	protected EntityManager entityManager;

	public static EstadoDAO getInstance() {
		if (instance == null) {
			instance = new EstadoDAO();
		}

		return instance;
	}

	private EstadoDAO() {
		entityManager = getEntityManager();
	}

	private EntityManager getEntityManager() {
		EntityManagerFactory factory = Persistence
				.createEntityManagerFactory(EnumTool.UNIDADE_PERSISTENCIA_ATUAL.getTexto());
		if (entityManager == null) {
			entityManager = factory.createEntityManager();
		}

		return entityManager;
	}

	public Estado getBySigla(final String sigla) {
		return entityManager.find(Estado.class, sigla);
	}

	@SuppressWarnings("unchecked")
	public List<Estado> findAll() {
		return entityManager.createQuery("FROM " + Estado.class.getName()).getResultList();
	}

	public void persist(Estado estado) {
		try {
			entityManager.persist(estado);
		} catch (Exception ex) {
			ex.printStackTrace();
			entityManager.getTransaction().rollback();
		}
	}

	public void persistOnMerge(Estado estado) {
		try {
			Estado estado2 = getBySigla(estado.getSigla());
			if (estado2 != null) {
				merge(estado);
			} else {
				persist(estado);
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			entityManager.getTransaction().rollback();
		}
	}

	public void merge(Estado estado) {
		try {
			entityManager.merge(estado);
		} catch (Exception ex) {
			ex.printStackTrace();
			entityManager.getTransaction().rollback();
		}
	}

	public void remove(Estado Estado) {
		try {
			Estado = entityManager.find(Estado.class, Estado.getSigla());
			entityManager.remove(Estado);
		} catch (Exception ex) {
			ex.printStackTrace();
			entityManager.getTransaction().rollback();
		}
	}

	public void removeBySigla(final String sigla) {
		try {
			Estado estado = getBySigla(sigla);
			if (estado != null) {
				remove(estado);
			} else {
				System.out.println(EnumTool.NAO_FORAM_ENCONTRADOS_REGISTROS.getTexto());
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	public void removeByNome(final String nome) {
		try {
			Estado estado = getByNome(nome);
			if (estado != null) {
				remove(estado);
			} else {
//                System.out.println(EnumTool.NAO_FORAM_ENCONTRADOS_REGISTROS.getTexto());
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	public Estado getByNome(String nome) {
		try {
			TypedQuery<Estado> query = entityManager.createQuery("select v from Estado v where v.nome = :nome",
					Estado.class);
			query.setParameter("nome", nome);
			Estado estado = query.getSingleResult();
			System.out.println(EnumTool.ESTADO.getTexto() + ": " + estado.getNome());
			return estado;
		} catch (Exception e) {
			System.out.println(EnumTool.NAO_FORAM_ENCONTRADOS_REGISTROS.getTexto());
		}
		return null;
	}

}
