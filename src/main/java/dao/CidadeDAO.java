package dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;

import domain.Cidade;
import enumeracao.EnumTool;

public class CidadeDAO {

    /** Criação do dao - Para Teste de Criação atráves de um dao */

    private static CidadeDAO instance;
    protected EntityManager entityManager;

    public static CidadeDAO getInstance(){
        if (instance == null){
            instance = new CidadeDAO();
        }

        return instance;
    }

     public CidadeDAO() {
        entityManager = getEntityManager();
    }

    private EntityManager getEntityManager() {
        EntityManagerFactory factory = Persistence.createEntityManagerFactory(EnumTool.UNIDADE_PERSISTENCIA_ATUAL.getTexto());
        if (entityManager == null) {
            entityManager = factory.createEntityManager();
        }

        return entityManager;
    }

    public Cidade getBySigla(final String sigla) {
        return entityManager.find(Cidade.class, sigla);
    }

    @SuppressWarnings("unchecked")
    public List<Cidade> findAll() {
        return entityManager.createQuery("FROM " + Cidade.class.getName()).getResultList();
    }

    public void persist(Cidade cidade) {
        try {
            entityManager.persist(cidade);
        } catch (Exception ex) {
            ex.printStackTrace();
            entityManager.getTransaction().rollback();
        }
    }

    public void persistOnMerge(Cidade cidade) {
        try {
            Cidade cidade2 = getBySigla(cidade.getSigla());
            if(cidade2 != null) {
                merge(cidade);
            } else {
                persist(cidade);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            entityManager.getTransaction().rollback();
        }
    }

    public void merge(Cidade cidade) {
        try {
            entityManager.merge(cidade);
             } catch (Exception ex) {
            ex.printStackTrace();
            entityManager.getTransaction().rollback();
        }
    }

    public void remove(Cidade Cidade) {
        try {
            Cidade = entityManager.find(Cidade.class, Cidade.getSigla());
            entityManager.remove(Cidade);
             } catch (Exception ex) {
            ex.printStackTrace();
            entityManager.getTransaction().rollback();
        }
    }

    public void removeBySigla(final String sigla) {
        try {
            Cidade cidade = getBySigla(sigla);
            if(cidade != null) {
                remove(cidade);
            } else {
                System.out.println(EnumTool.NAO_FORAM_ENCONTRADOS_REGISTROS.getTexto());
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void removeByNome(final String nome) {
        try {
            Cidade cidade = getByNome(nome);
            if(cidade != null) {
                remove(cidade);
            } else {
//                System.out.println(EnumTool.NAO_FORAM_ENCONTRADOS_REGISTROS.getTexto());
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public Cidade getByNome(String nome) {
        try {
            TypedQuery<Cidade> query = entityManager.createQuery("select c from Cidade c where c.nome = :nome", Cidade.class);
            query.setParameter("nome", nome);
            Cidade cidade = query.getSingleResult();
            System.out.println(EnumTool.CIDADE.getTexto() + ": " + cidade.getNome());
            return cidade;
        }catch (Exception e) {
        System.out.println(EnumTool.NAO_FORAM_ENCONTRADOS_REGISTROS.getTexto());
        }
        return null;
    }

}
