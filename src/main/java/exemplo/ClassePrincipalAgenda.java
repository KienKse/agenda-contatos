package exemplo;

import dao.CidadeDAO;
import dao.ContatoDAO;
import dao.EnderecoDAO;
import domain.Cidade;
import domain.Contato;
import domain.Endereco;
import domain.Estado;
import enumeracao.EnumTool;
import enumeracao.TipoEnderecoEnum;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;

public class ClassePrincipalAgenda {

	private static Integer opcao = 0;
	private static Integer consultaOpc = 0;
	private static Integer excluirOpc = 0;
	private static Integer quantidadeRegistros = 0;
	private static String tipo;
	private static boolean agenda = true;
	private static boolean excluir = true;
	private static boolean newEmail = true;
	private static boolean newTelefone = true;
	private static boolean newEndereco = true;
	private static boolean newTipoEndereco = true;
	private static List<String> emails = new ArrayList<>();
	private static List<String> telefones = new ArrayList<>();
	private static List<Endereco> enderecos = new ArrayList<>();
	private static List<Cidade> cidadesPersist = new ArrayList<>();
	private static Endereco endereco = new Endereco();
	private static Cidade cidade = new Cidade();
	private static Estado estado = new Estado();
	private static ContatoDAO contatoDAO = new ContatoDAO();
	private static CidadeDAO cidadeDAO = new CidadeDAO();
	private static EntityManagerFactory emf = null;
	private static EntityManager em = null;

	public static void main(String[] args) {

		emf = Persistence.createEntityManagerFactory(EnumTool.UNIDADE_PERSISTENCIA_ATUAL.getTexto());
		em = emf.createEntityManager();

		Scanner sc = new Scanner(System.in);
		apresentacaoPrincipalAgenda();

		gerenciarAgenda(sc, em);
	}

	private static void apresentacaoPrincipalAgenda() {
		System.out.println(EnumTool.BEM_VINDO.getTexto());
		System.out.println(EnumTool.LINE_SEPARATOR.getTexto());
		System.out.println(EnumTool.DIGITE.getTexto());
		System.out.println(EnumTool.OPCOES.getTexto());
	}

	private static void gerenciarAgenda(Scanner sc, EntityManager em) {
		String tipo;
		int consultaOpc;
		while (agenda) {
			redefinirBoleanos();
			opcao = sc.nextInt();
			switch (opcao) {
			case 1:
				Contato contato = new Contato();
				endereco = new Endereco();
				enderecos = new ArrayList<Endereco>();

				System.out.println(EnumTool.CADASTRO_OPCAO.getTexto());
				while (newEndereco) {
					System.out.println(
							"Deseja Cadastrar um novo Endereco para o contato? Digite 1 para SIM e 2 para NAO ");
					if (sc.nextInt() == 1) {

						System.out.println(EnumTool.INFORME.getTexto() + EnumTool.NOME.getTexto() + " -> "
								+ EnumTool.BAIRRO.getTexto());
						sc = new Scanner(System.in);

						endereco.setBairro(sc.nextLine());

						System.out.println(EnumTool.INFORME.getTexto() + EnumTool.NOME.getTexto() + " -> "
								+ EnumTool.LOGRADOURO.getTexto());
						sc = new Scanner(System.in);

						endereco.setLogradouro(sc.nextLine());

						System.out.println(EnumTool.INFORME.getTexto() + EnumTool.NUMERO.getTexto() + " -> "
								+ EnumTool.ENDERECO.getTexto());
						endereco.setNumero(sc.nextInt());

						while (newTipoEndereco) {
							System.out.println(
									"Digite o tipo do Endereco que deseja cadastrar: C para Comercial e R para Residencial");
							tipo = sc.next();
							if (tipo.equalsIgnoreCase(TipoEnderecoEnum.COMERCIAL.getSigla())) {
								endereco.setTipoEnderecoEnum(TipoEnderecoEnum.COMERCIAL);
								newTipoEndereco = false;
							} else if (tipo.equalsIgnoreCase(TipoEnderecoEnum.RESIDENCIAL.getSigla())) {
								endereco.setTipoEnderecoEnum(TipoEnderecoEnum.RESIDENCIAL);
								newTipoEndereco = false;
							} else {
								System.out.println(EnumTool.OPCAO_INVALIDA.getTexto());
							}
						}

						cidade = new Cidade();
						System.out.println(EnumTool.INFORME.getTexto() + EnumTool.CIDADE.getTexto() + " -> "
								+ EnumTool.NOME.getTexto());
						sc = new Scanner(System.in);
						String nome = sc.nextLine();

						Cidade cidadeBanco = cidadeDAO.getInstance().getByNome(nome);

						if (cidadeBanco == null) {
							em.getTransaction().begin();

							cidade.setNome(nome);
							System.out.println(EnumTool.INFORME.getTexto() + EnumTool.CIDADE.getTexto() + " -> "
									+ EnumTool.SIGLA.getTexto());
							System.out.println(EnumTool.TRES_CARACTERES.getTexto());
							cidade.setSigla(sc.next());

							estado = new Estado();
							System.out.println(EnumTool.INFORME.getTexto() + EnumTool.ESTADO.getTexto() + " -> "
									+ EnumTool.NOME.getTexto());
							sc = new Scanner(System.in);
							estado.setNome(sc.nextLine());
							System.out.println(EnumTool.INFORME.getTexto() + EnumTool.ESTADO.getTexto() + " -> "
									+ EnumTool.SIGLA.getTexto());
							System.out.println(EnumTool.DOIS_CARACTERES.getTexto());
							estado.setSigla(sc.next());

							em.persist(estado);
							cidade.setEstado(estado);
							em.persist(cidade);

							em.getTransaction().commit();

						} else {
							cidade = cidadeBanco;
						}

						endereco.setCidade(cidade);

						enderecos.add(endereco);

					} else {
						newEndereco = false;
					}
				}
				em.getTransaction().begin();

				if (enderecos != null && !enderecos.isEmpty()) {
					contato.setEnderecos(enderecos);
				}

				extrairListaDeAtributosDeContato(sc, newTelefone, EnumTool.TELEFONES, EnumTool.TELEFONE, telefones);
				contato.setTelefones(telefones);

				extrairListaDeAtributosDeContato(sc, newEmail, EnumTool.EMAILS, EnumTool.EMAIL, emails);
				contato.setEmails(emails);

				System.out.println(
						EnumTool.INFORME.getTexto() + EnumTool.NOME.getTexto() + " -> " + EnumTool.CONTATO.getTexto());
				sc = new Scanner(System.in);
				contato.setNome(sc.nextLine());

				em.persist(contato);

				em.getTransaction().commit();

				retornarAoMenoPrincipal();
				break;

			case 2:
				System.out.println(EnumTool.CONSULTA_OPCAO.getTexto());
				consultaOpc = sc.nextInt();

				switch (consultaOpc) {
				case 1:
					try {
						System.out.println(EnumTool.INFORME.getTexto() + "ID -> " + EnumTool.CONTATO.getTexto());
						contato = contatoDAO.getById(sc.nextLong());
						retornarContato(contato);
					} catch (InputMismatchException e) {
						carecterIncorretoRetornarAoMenuPrincipal();
					}
					break;

				case 2:
					System.out.println(
							EnumTool.INFORME.getTexto() + EnumTool.NOME + " -> " + EnumTool.CONTATO.getTexto());
					sc = new Scanner(System.in);
					String nome = sc.nextLine();
					contato = contatoDAO.getByNome(nome);
					retornarContato(contato);
					break;

				case 3:
					List<Contato> contatos = contatoDAO.findAll();
					retornarContato(contatos);
					break;

				default:
					System.out.println(EnumTool.OPCAO_INVALIDA.getTexto());
					break;
				}
				break;
			case 3:
				while (excluir) {
					System.out.println(EnumTool.EXCLUIR);
					System.out.println("- 1 " + EnumTool.CONTATO_PELO_ID.getTexto() + "\n- 2 "
							+ EnumTool.CONTATO_PELO_NOME.getTexto() + "\n- 3 " + EnumTool.VOLTAR_MENU.getTexto());
					excluirOpc = sc.nextInt();

					switch (excluirOpc) {
					case 1:
						System.out.println(EnumTool.INFORME.getTexto() + "ID -> " + EnumTool.CONTATO.getTexto());
						contatoDAO.removeById(sc.nextLong());
						break;

					case 2:
						System.out.println(
								EnumTool.INFORME.getTexto() + EnumTool.NOME + " -> " + EnumTool.CONTATO.getTexto());
						sc = new Scanner(System.in);
						contatoDAO.removeByNome(sc.nextLine());
						break;
					case 3:
						excluir = false;
						retornarAoMenoPrincipal();
						break;
					default:
						System.out.println(EnumTool.OPCAO_INVALIDA.getTexto());
						excluirOpc = 0;
						break;
					}
				}
				break;
			case 4:
				System.out.println(EnumTool.ATUALIZAR);
				System.out.println(EnumTool.INFORME.getTexto() + "ID -> " + EnumTool.CONTATO.getTexto());
				contato = contatoDAO.getById(sc.nextLong());
				System.out.println(
						"O que deseja atualizar do contato? digite 1 para nome, 2 para e-mails, 3 para telefones e 4 para endereco ");
				switch (sc.nextInt()) {
				case 1:
					System.out.println(EnumTool.INFORME.getTexto() + EnumTool.NOME.getTexto() + " -> "
							+ EnumTool.CONTATO.getTexto());
					sc = new Scanner(System.in);
					contato.setNome(sc.nextLine());

					ContatoDAO.getInstance().merge(contato);
					retornarAoMenoPrincipal();
					break;
				case 2:
					System.out.println(EnumTool.EMAILS.getTexto());
					extrairListaDeAtributosDeContato(sc, newEmail, EnumTool.EMAILS, EnumTool.EMAIL, emails);
					contato.setEmails(emails);
					ContatoDAO.getInstance().merge(contato);
					retornarAoMenoPrincipal();
					break;
				case 3:
					System.out.println(EnumTool.TELEFONES.getTexto());
					extrairListaDeAtributosDeContato(sc, newTelefone, EnumTool.TELEFONES, EnumTool.TELEFONE, telefones);
					contato.setTelefones(telefones);
					ContatoDAO.getInstance().merge(contato);
					retornarAoMenoPrincipal();
					break;
				case 4:
					System.out.println(
							"Deseja Editar um endereco ou cadastrar um novo? digite \n - 1 para EDITAR e \n - 2 para CADASTRAR");
					int updateOption = sc.nextInt();
					if (updateOption == 1) {

						if (contatoDAO.verificarEnderecoExistente(contato)) {

							System.out.println(EnumTool.ENDERECO.getTexto());
							enderecos = contato.getEnderecos();

							System.out.println(EnumTool.ID_EDITAR.getTexto());
							Endereco e = enderecos.get(sc.nextInt());

							System.out.println(EnumTool.NOVO_EDITAR.getTexto());
							sc = new Scanner(System.in);
							e.setBairro(sc.nextLine());

							System.out.println(EnumTool.LOGRADOURO_EDITAR.getTexto());
							sc = new Scanner(System.in);
							e.setLogradouro(sc.nextLine());

							try {
								System.out.println(EnumTool.NOVO_NUMERO_ENDERECO.getTexto());
								e.setNumero(sc.nextInt());
							} catch (InputMismatchException y) {
								carecterIncorretoRetornarAoMenuPrincipal();
							}



							System.out.println(
									"Digite o tipo do Endereco que deseja cadastrar: C para Comercial e R para Residencial");
							tipo = sc.next();
							if (tipo.equalsIgnoreCase(TipoEnderecoEnum.COMERCIAL.getSigla())) {
								e.setTipoEnderecoEnum(TipoEnderecoEnum.COMERCIAL);
							} else if (tipo.equalsIgnoreCase(TipoEnderecoEnum.COMERCIAL.getSigla())) {
								e.setTipoEnderecoEnum(TipoEnderecoEnum.RESIDENCIAL);
							} else {
								System.out.println(EnumTool.OPCAO_INVALIDA.getTexto());
							}

							System.out.println(EnumTool.NOVO_NOME_CIDADE.getTexto());
							cidade = new Cidade();
							cidade.setNome(sc.next());

							System.out.println(EnumTool.NOVA_SIGLA_CIDADE.getTexto());
							cidade.setSigla(sc.next());

							System.out.println(EnumTool.INFORME.getTexto() + EnumTool.NOME.getTexto() + " -> "
									+ EnumTool.ESTADO.getTexto());
							estado = new Estado();
							estado.setNome(sc.next());

							System.out.println(
									EnumTool.INFORME.getTexto() + EnumTool.SIGLA.getTexto() + " -> " + EnumTool.ESTADO);
							estado.setSigla(sc.next());

							cidade.setEstado(estado);
							e.setCidade(cidade);

						} else {
							opcao = 0;
							retornarAoMenoPrincipal();
						}

					} else if(updateOption == 2) {
						enderecos = new ArrayList<Endereco>();
						endereco = new Endereco();
						System.out.println(EnumTool.INFORME.getTexto() + EnumTool.NOME.getTexto() + " -> "
								+ EnumTool.BAIRRO.getTexto());

						sc = new Scanner(System.in);
						endereco.setBairro(sc.nextLine());

						System.out.println(EnumTool.INFORME.getTexto() + EnumTool.NOME.getTexto() + " -> "
								+ EnumTool.LOGRADOURO.getTexto());
						sc = new Scanner(System.in);
						endereco.setLogradouro(sc.nextLine());

						System.out.println(EnumTool.INFORME.getTexto() + EnumTool.NUMERO.getTexto() + " -> "
								+ EnumTool.ENDERECO.getTexto());
						endereco.setNumero(sc.nextInt());

						while (newTipoEndereco) {
							System.out.println(
									"Digite o tipo do Endereco que deseja cadastrar: C para Comercial e R para Residencial");
							tipo = sc.next();
							if (tipo.equalsIgnoreCase(TipoEnderecoEnum.COMERCIAL.getSigla())) {
								endereco.setTipoEnderecoEnum(TipoEnderecoEnum.COMERCIAL);
								newTipoEndereco = false;
							} else if (tipo.equalsIgnoreCase(TipoEnderecoEnum.RESIDENCIAL.getSigla())) {
								endereco.setTipoEnderecoEnum(TipoEnderecoEnum.RESIDENCIAL);
								newTipoEndereco = false;
							} else {
								System.out.println(EnumTool.OPCAO_INVALIDA.getTexto());
							}
						}

						System.out.println(EnumTool.INFORME.getTexto() + EnumTool.CIDADE.getTexto() + " -> "
								+ EnumTool.NOME.getTexto());
						sc = new Scanner(System.in);
						String nome = sc.nextLine();

						Cidade cidadeBanco = cidadeDAO.getInstance().getByNome(nome);

						if (cidadeBanco == null) {
							em.getTransaction().begin();

							cidade.setNome(nome);
							System.out.println(EnumTool.INFORME.getTexto() + EnumTool.CIDADE.getTexto() + " -> "
									+ EnumTool.SIGLA.getTexto());
							System.out.println(EnumTool.TRES_CARACTERES.getTexto());
							cidade.setSigla(sc.next());

							estado = new Estado();
							System.out.println(EnumTool.INFORME.getTexto() + EnumTool.ESTADO.getTexto() + " -> "
									+ EnumTool.NOME.getTexto());
							sc = new Scanner(System.in);
							estado.setNome(sc.nextLine());
							System.out.println(EnumTool.INFORME.getTexto() + EnumTool.ESTADO.getTexto() + " -> "
									+ EnumTool.SIGLA.getTexto());
							System.out.println(EnumTool.DOIS_CARACTERES.getTexto());
							estado.setSigla(sc.next());

							em.persist(estado);
							cidade.setEstado(estado);
							em.persist(cidade);

							em.getTransaction().commit();

						} else {
							cidade = cidadeBanco;
						}

						cidade.setEstado(estado);

						endereco.setCidade(cidade);
						if (contato.getEnderecos() != null && !contato.getEnderecos().isEmpty()) {
							contato.getEnderecos().add(endereco);
						} else {
							enderecos.add(endereco);
							contato.setEnderecos(enderecos);
						}

						EnderecoDAO.getInstance().persist(endereco);

						ContatoDAO.getInstance().merge(contato);
						retornarAoMenoPrincipal();
					} else {
						System.out.println(EnumTool.OPCAO_INVALIDA.getTexto());
						opcao = 0;
						retornarAoMenoPrincipal();
					}
					break;
				}
				break;
			case 5:
				System.out.println(EnumTool.GOOD_BYE.getTexto());
				agenda = false;
				break;
			default:
				System.out.println(EnumTool.OPCAO_INVALIDA.getTexto());
			}
		}
	}

	private static void carecterIncorretoRetornarAoMenuPrincipal() {
		System.out.println(EnumTool.CARACTER_INCORRETO.getTexto());
		em.getTransaction().rollback();
		opcao = 0;
		retornarAoMenoPrincipal();
	}

	private static void redefinirBoleanos() {
		newEndereco = true;
		newTipoEndereco = true;
		newEmail = true;
		newTelefone = true;
		excluir = true;
	}

	private static void persistirOrMergearConteudos(EnderecoDAO instance, List<Endereco> enderecos) {
		if (enderecos != null && !enderecos.isEmpty()) {
			for (Endereco endereco : enderecos) {
				instance.persistOnMerge(endereco);
			}
		}
	}

	private static void persistirOrMergearConteudos(CidadeDAO instance, List<Cidade> cidades) {
		if (cidades != null && !cidades.isEmpty()) {
			for (Cidade cidade : cidades) {
				instance.persistOnMerge(cidade);
			}
		}
	}

	private static void extrairListaDeAtributosDeContato(Scanner sc, boolean boleano, EnumTool atributos,
			EnumTool atributo, List lista) {
		while (boleano) {
			System.out.println(
					EnumTool.CADASTRAR.getTexto() + " " + atributos.getTexto() + ", Selecione uma das opcoes:");
			System.out.println("1) Sim");
			System.out.println("2) Nao");
			int opt = sc.nextInt();
			if (opt == 1) {
				System.out.println(EnumTool.QUANTIDADE_REGISTROS.getTexto());
				quantidadeRegistros = (sc.nextInt());
				for (int i = 0; i < quantidadeRegistros; i++) {
					System.out.println(EnumTool.INFORME.getTexto() + atributo.getTexto());
					lista.add(sc.next());
				}
				boleano = false;
			} else if (opt == 2) {
				boleano = false;
			} else {
				System.out.println(EnumTool.OPCAO_INVALIDA.getTexto());
			}
		}
	}

	private static void retornarContato(Contato contato) {
		if (contato != null) {
			System.out.println("Contato: ");
			System.out.println(contato.toString());
			retornarAoMenoPrincipal();
		} else {
			System.out.println(EnumTool.NAO_FORAM_ENCONTRADOS_REGISTROS.getTexto());
			retornarAoMenoPrincipal();
		}
	}

	private static void retornarContato(List<Contato> contatos) {
		if (contatos != null && !contatos.isEmpty()) {
			System.out.println("Contatos: ");
			for (Contato contato : contatos) {
				System.out.println(contato.toString());
			}
			retornarAoMenoPrincipal();
		} else {
			System.out.println(EnumTool.NAO_FORAM_ENCONTRADOS_REGISTROS.getTexto());
			retornarAoMenoPrincipal();
		}
	}

	private static void retornarAoMenoPrincipal() {
		opcao = 0;
		apresentacaoPrincipalAgenda();
	}
}
