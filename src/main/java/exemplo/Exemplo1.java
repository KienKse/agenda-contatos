package exemplo;

import domain.Cidade;
import domain.Contato;
import domain.Endereco;
import domain.Estado;
import enumeracao.EnumTool;
import enumeracao.TipoEnderecoEnum;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.util.ArrayList;
import java.util.Arrays;

public class Exemplo1 {

	/**
	 * 
	 * Desenvolver uma agenda de contatos (nome, telefones, endereços
	 * (tipo(comercial, residencial), logradouro, cidade, estado, etc), emails);
	 * 
	 * Sua agenda deve ter as seguintes funcionalidades: cadastrar consultar
	 * atualizar excluir
	 * 
	 * Interface livre (web, mobile, console, swing); OBRIGATÓRIO: utilizar JPA com
	 * Hibernate.
	 * 
	 */

	public static void main(String[] args) {
		EntityManagerFactory emf = null;
		try {

			emf = Persistence.createEntityManagerFactory(EnumTool.UNIDADE_PERSISTENCIA_ATUAL.getTexto());
			EntityManager em = emf.createEntityManager();

			em.getTransaction().begin();
			
			Contato contatoVazio = new Contato("vazio");
			em.persist(contatoVazio);

			Endereco endereco = new Endereco();
			endereco.setBairro("Bairro X");
			endereco.setLogradouro("Logradouro X");
			endereco.setTipoEnderecoEnum(TipoEnderecoEnum.COMERCIAL);
			endereco.setNumero(1);

			Cidade cidade = new Cidade("SSA", "Salvador", new Estado("BA", "Bahia"));

			em.persist(cidade);

			endereco.setCidade(cidade);

			Contato contatoNutrido = new Contato("Adalberto", new ArrayList<String>(),
					new ArrayList<Endereco>(Arrays.asList(endereco)), new ArrayList<String>(Arrays.asList()));
			em.persist(contatoNutrido);

			Contato contatoVazio2 = new Contato("vazio2");
			em.persist(contatoVazio2);

			Contato contatoNutrido2 = new Contato("Marcos", new ArrayList<String>(),
					null, new ArrayList<String>(Arrays.asList()));
			em.persist(contatoNutrido2);

			em.getTransaction().commit();

			em.close();

		} catch (Exception e) {
			e.printStackTrace();
			System.out.print("Falha no programa principal: " + e);
		} finally {
			if (emf != null) {
				emf.close();
			}
		}
	}
}
