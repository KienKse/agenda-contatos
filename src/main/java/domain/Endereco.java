package domain;

import converter.TipoEnderecoConverter;
import enumeracao.TipoEnderecoEnum;

import javax.persistence.*;

@Entity
@SequenceGenerator(name = "seq_endereco", sequenceName = "seq_endereco")
@Table(name = "endereco")
public class Endereco {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_endereco")
	private Long id;

	@Column(name = "tipo_endereco", columnDefinition = "CHAR(1)", nullable = false)
	@Convert(converter = TipoEnderecoConverter.class)
	private TipoEnderecoEnum tipoEnderecoEnum;

	@Column(length = 40, nullable = false)
	private String logradouro;

	@Column(columnDefinition = "NUMERIC(4,0)", nullable = false)
	private Integer numero;

	@Column(length = 30, nullable = false)
	private String bairro;

	@ManyToOne(cascade={CascadeType.PERSIST,CascadeType.MERGE})
	private Cidade cidade;

	public Endereco() {
		// Empty Constructor
	}

	public Endereco(TipoEnderecoEnum tipoEnderecoEnum, String logradouro, Integer numero, String bairro, Cidade cidade) {
		this.tipoEnderecoEnum = tipoEnderecoEnum;
		this.logradouro = logradouro;
		this.numero = numero;
		this.bairro = bairro;
		this.cidade = cidade;
	}

	public Long getId() {
		return id;
	}

	public TipoEnderecoEnum getTipoEnderecoEnum() {
		return tipoEnderecoEnum;
	}

	public void setTipoEnderecoEnum(TipoEnderecoEnum tipoEnderecoEnum) {
		this.tipoEnderecoEnum = tipoEnderecoEnum;
	}

	public String getLogradouro() {
		return logradouro;
	}

	public void setLogradouro(String logradouro) {
		this.logradouro = logradouro;
	}

	public Integer getNumero() {
		return numero;
	}

	public void setNumero(Integer numero) {
		this.numero = numero;
	}

	public String getBairro() {
		return bairro;
	}

	public void setBairro(String bairro) {
		this.bairro = bairro;
	}

	public Cidade getCidade() {
		return cidade;
	}

	public void setCidade(Cidade cidade) {
		this.cidade = cidade;
	}

	@Override
	public String toString() {
		return "Endereco{" +
				"id=" + id + '\'' +
				", tipoEnderecoEnum=" + tipoEnderecoEnum.getSigla() +
				", logradouro='" + logradouro + '\'' +
				", numero=" + numero +
				", bairro='" + bairro + '\'' +
				", cidade=" + cidade + '\'' +
				", estado=" + cidade.getEstado().getNome() + '\'' +
				'}';
	}
}
