package domain;

import javax.persistence.*;
import java.util.List;

@Entity
@SequenceGenerator(name = "seq_contato", sequenceName = "seq_contato")
@Table(name = "contato")
public class Contato {

    @Id
//	@SequenceGenerator(name = "contato_generator", sequenceName = "contato_sequence", allocationSize = 1)
//	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "contato_generator")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_contato")
	private Long id;

    @Column(length = 40, nullable = false)
	private String nome;

    @ElementCollection()
    @CollectionTable(name = "telefones_contato", joinColumns = @JoinColumn(name = "nome"))
    @Column(name = "telefones", nullable = false, length = 16)
	private List<String> telefones;

    @OneToMany(targetEntity = Endereco.class,
            fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private List<Endereco> enderecos;

    @ElementCollection
    @CollectionTable(name = "emails_contato", joinColumns = @JoinColumn(name = "nome"))
    @Column(name = "emails", length = 40)
    private List<String> emails;

	public Contato() {
		// Empty Constructor
	}

	public Contato(String nome) {
		this.nome = nome;
	}

	public Contato(String nome, List<String> telefones, List<Endereco> enderecos, List<String> emails) {
		super();
		this.nome = nome;
		this.telefones = telefones;
		this.enderecos = enderecos;
		this.emails = emails;
	}

	public Long getId() {
		return id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public List<String> getTelefones() {
		return telefones;
	}

	public void setTelefones(List<String> telefones) {
		this.telefones = telefones;
	}

	public List<Endereco> getEnderecos() {
		return enderecos;
	}

	public void setEnderecos(List<Endereco> enderecos) {
		this.enderecos = enderecos;
	}

	public List<String> getEmails() {
		return emails;
	}

	public void setEmails(List<String> emails) {
		this.emails = emails;
	}

	@Override
	public String toString() {
		return "Contato{" +
				"id = " + id +
				", nome =" + nome +
				", telefones =" + telefones +
				", enderecos =" + enderecos +
				", emails =" + emails +
				'}';
	}
}