package domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "estado")
public class Estado {

	@Id
	@Column(columnDefinition = "CHAR(2)", nullable = false)
	private String sigla;
	
	@Column(length = 40, nullable = false)
	private String nome;

	public Estado() {
		// Empty Constructor
	}

	public Estado(String sigla, String nome) {
		this.sigla = sigla;
		this.nome = nome;
	}

	public String getSigla() {
		return sigla;
	}

	public void setSigla(String sigla) {
		this.sigla = sigla;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	@Override
	public String toString() {
		return "Estado{" +
				"sigla='" + sigla + '\'' +
				", nome='" + nome + '\'' +
				'}';
	}
}
