package domain;

import javax.persistence.*;

@Entity
@Table(name = "cidade")
public class Cidade {

	@Id
	@Column(columnDefinition = "CHAR(3)", nullable = false)
	private String sigla;
	
	@Column(length = 40, nullable = false)
	private String nome;
	
	@ManyToOne(cascade={CascadeType.PERSIST,CascadeType.MERGE})
	private Estado estado;

	public Cidade() {
		// Empty Constructor
	}

	public Cidade(String sigla, String nome) {
		this.sigla = sigla;
		this.nome = nome;
	}

	public Cidade(String sigla, String nome, Estado estado) {
		this.sigla = sigla;
		this.nome = nome;
		this.estado = estado;
	}

	public String getSigla() {
		return sigla;
	}

	public void setSigla(String sigla) {
		this.sigla = sigla;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Estado getEstado() {
		return estado;
	}

	public void setEstado(Estado estado) {
		this.estado = estado;
	}

	@Override
	public String toString() {
		return "Cidade{" +
				"sigla='" + sigla + '\'' +
				", nome='" + nome + '\'' +
				", estado=" + estado +
				'}';
	}
}