package domain;

public enum TipoVeiculoEnum {

    CARGA (“CR”), PASSEIO (“PS”);


    TipoVeiculoEnum(String codigo) {
        this.codigo = codigo;
    }

    private String codigo;

    public String getCodigo() {
        return codigo;
    }
}
