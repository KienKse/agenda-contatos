package enumeracao;

public enum EnumTool {

	/** Adicionar constantes que serao utilizadas ao longo da aplicacao */

	UNIDADE_PERSISTENCIA_ATUAL("AJUSTAR_UNIDADE_PERSISTENCIA"),
	BEM_VINDO("Bem Vindo, ao Sistema!"),
	DIGITE("Digite o nomero abaixo para informar a opcao desejada e em seguida precione enter:"),
	OPCOES("Digite 1 para CADASTRAR 2 para CONSULTAR 3 para EXCLUIR 4 para ATUALIZAR ou 5 para SAIR"),
	QUANTIDADE_REGISTROS("Informe a quantidade de registros para o tipo de cadastro selecionado "),

	LINE_SEPARATOR("\n-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-"),

	CONTATO("Contato"),
	ESTADO("Estado"),
	ENDERECO("Endereco"),
	CIDADE("Cidade"),

	CONSULTA("Consultar"),
	ATUALIZAR("Atualizar"),
	EXCLUIR("Excluir"),
	INCLUIR("Incluir"),
	CADASTRAR("Cadastrar"),
	VOLTAR_MENU("Retornar ao Menu Principal"),

	CONTATO_PELO_ID("pelo ID"),
	CONTATO_PELO_NOME("pelo NOME"),

	TRES_CARACTERES("Tres Caracteres -> Ex: SSA"),
	DOIS_CARACTERES("Dois Caracteres -> Ex: BA"),

	INFORME("Informe -> "),
	NOME("Nome"),
	SIGLA("Sigla"),
	LOGRADOURO("Logradouro"),
	NUMERO("Numero"),
	EMAIL("E-mail"),
	TELEFONE("Telefone"),
	BAIRRO("Bairro"),
	TELEFONES("Lista de Telefones"),
	ENDERECOS("Lista de Enderecos"),
	EMAILS("Lista de Emails"),

	NOVO_EMAIL("Deseja Cadastrar um novo e-mail para o contato? Digite 1 para sim e 2 para nao "),
	NOVO_EMAIL_CONTATO("Digite o novo e-mail do contato: "),
	NAO_FORAM_ENCONTRADOS_REGISTROS("\nNao foram encontrados registros com os paremetros requeridos\n"),
	OPCAO_INVALIDA("Opcao Informada invalida! Digite novamente as opcoes..."),
	CARACTER_INCORRETO("Tipo de entrada diferente do esperado resultado em um caractere inv�lido...\nRetornando ao menu principal!"),
	ID_EDITAR("Digite o id do valor que deseja editar: "),
	NOVO_EDITAR("Digite o novo nome do bairro: "),
	LOGRADOURO_EDITAR("Digite o nome do novo Logradouro: "),
	NOVO_NUMERO_ENDERECO("Digite o novo numero: "),
	NOVO_NOME_CIDADE("Digite o nome da cidade: "),
	NOVA_SIGLA_CIDADE("Digite a sigla da cidade: "),
	GOOD_BYE("\n\nAdeus, Ate a proxima...\n\n"),

	CONSULTA_OPCAO("Caso deseje consultar o Contato pelo ID digite 1 caso deseje pelo NOME digite 2 ou TODOS digite 3"),
	INCLUIR_OPCAO("O que voce deseja incluir?"),
	EXCLUIR_OPCAO("Deseja excluir um registro?"),
	CADASTRO_OPCAO("Cadastro de novo contato");

	EnumTool(String texto) {
		this.texto = texto;
	}
	
	private String texto;

	public String getTexto() {
		return texto;
	}
}
