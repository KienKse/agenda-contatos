package enumeracao;

public enum TipoEnderecoEnum {
	
	COMERCIAL("C"), RESIDENCIAL("R");

	TipoEnderecoEnum(String sigla) {
		this.sigla = sigla;
	}
	
	private String sigla;

	public String getSigla() {
		return sigla;
	}

	public static TipoEnderecoEnum valorOfSigla(String sigla) {
		for (TipoEnderecoEnum tipoEnderecoEnum : values()) {
			if (tipoEnderecoEnum.getSigla().equals(sigla)) {
				return tipoEnderecoEnum;
			}
		}
		throw new IllegalArgumentException("Sigla Endereco nao encontrada =" + sigla);
	}

}
